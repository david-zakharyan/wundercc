# WunderCC #

Application downlaods and displays locations' data.

### Minimum Deployment Target iOS 9.0 ###

### Application set up: Is ready to run ###

* Project uses dependency manager CocoaPods (CocoaPods installation instructions can be found at https://cocoapods.org)
* Pods are include in source control

### Application Dependencies ###

* Alamofire freezed version 4.7.0 (https://github.com/Alamofire/Alamofire)
* ObjectMapper freezed version 3.1 (https://github.com/Hearst-DD/ObjectMapper)
* Realm freezed version 3.2.0 (https://realm.io)

### Have a question? ###

Please feel free to contact with me

* dzakharyan@gmail.com
* +(49)15252885879
