//
//  APIReaquestHelper.swift
//  WunderCC
//
//  Created by Davit Zakharyan on 3/18/18.
//  Copyright © 2018 Davit Zakharyan. All rights reserved.
//


typealias API = APIRequest

/// Provides basic supporting functionality for endppint requests
internal enum APIRequest {
    private static let basePath = "https://s3-us-west-2.amazonaws.com/wunderbucket/"
    
    case locations
    
    private var method: String {
        switch self {
        case .locations:
            return "locations.json"
        }
    }
    
    var path: String {
        return API.basePath + method
    }
    
    var contenType : String {
        switch self {
        case .locations:
            return "text/plain"
        }
    }
    
    var predefinedErrorMessage: String {
        switch self {
        case .locations:
            return "Couldn't load locations."
        }
    }
    
}
