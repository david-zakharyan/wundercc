//
//  ServiceModelDataBase.swift
//  WunderCC
//
//  Created by Davit Zakharyan on 3/17/18.
//  Copyright © 2018 Davit Zakharyan. All rights reserved.
//

import RealmSwift

// TODO: Migration
// TODO: Requires unit etst
// TODO: Try to separate zipcode and city for sorting , save them separatly
typealias SMDatabase = ServiceModelDataBase

class ServiceModelDataBase {
    
    internal func save(placemarks: [JSONPlacemark]) -> Bool {
        var realmCars: [RMCar] = []
        var realmLocations: [RMLocation] = []
        
        for JSON in placemarks {
            if let vin = JSON.vin, !vin.isEmpty,
               let locationIdentifer = JSON.name, !locationIdentifer.isEmpty {
                
                let realmCar                    = RMCar()
                realmCar.vin                    = vin
                realmCar.locationIdentifier     = JSON.name
                realmCar.engineType             = JSON.engineType
                realmCar.exterior               = JSON.exterior
                realmCar.interior               = JSON.interior
                realmCar.fuel                   = JSON.fuel
                
                let realmLocation       = RMLocation()
                realmLocation.address   = JSON.address
                realmLocation.name      = JSON.name
                if let coordinates = JSON.coordinates, coordinates.count > 1 {
                    realmLocation.longitude = coordinates[0]
                    realmLocation.latitude  = coordinates[1]
                }
                
                realmLocations.append(realmLocation)
                realmCars.append(realmCar)
            }
        }
        
        // FIXME: chrash me please :)
        let todoRealm = try! Realm()

        // FIXME: chrash me please :)
        try! todoRealm.write {
                todoRealm.add(realmCars, update: true)
                todoRealm.add(realmLocations, update: true)
        }
        
        return true
    }
}


