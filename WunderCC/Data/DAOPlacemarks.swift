//
//  DAOPlacemarks.swift
//  WunderCC
//
//  Created by Davit Zakharyan on 3/18/18.
//  Copyright © 2018 Davit Zakharyan. All rights reserved.
//

import ObjectMapper

//Although we use realms Objects now, but it won't be forever

/// Data access object for placmarsk
typealias DAOPlacemarks = DataAccessObjectPlacemarks

class DataAccessObjectPlacemarks {
    
    internal typealias completionBlock = (Bool, [JSONPlacemark]?) -> Void
    
    internal func placmarks(completion: @escaping completionBlock) {
        if hasValidLocalData() {
            completion(false, nil)
            
        } else {
            getRemotePlacemarks(completion: completion)
        }
    }
    
    //TODO: Implement
    /// Identifies whthere the database has valid data
    ///
    /// - Returns: ture if has false otherwise
    private func hasValidLocalData() -> Bool {
        return false
    }
    
    //TODO: Implement
    /// Returns placemark from the database
    ///
    /// - Returns: Placemarks form the database
    private func getLocalPlacemarks(completion: @escaping completionBlock) {
        
    }
    
    
    /// Requests placemark from the server
    ///
    /// - Parameter completion: json models of placemarks or nil.
    private func getRemotePlacemarks(completion: @escaping completionBlock) {
        SMNetwork.sharedInstance.request(APIEndpoit: API.locations) {(success, data) in
            if success {
                var JSONPlacemarksArray: [JSONPlacemark]?
                if let placemarksData = data as? [String: [AnyObject]], let placemarksArray = placemarksData["placemarks"]  {
                    JSONPlacemarksArray = []
                    for placemark in placemarksArray {
                        if let JSON = placemark as? [String: AnyObject] {
                            if let JSONPlacemark = Mapper<JSONPlacemark>().map(JSON: JSON) {
                                JSONPlacemarksArray?.append(JSONPlacemark)
                            }
                        }
                    }
                }
                completion(true, JSONPlacemarksArray)
            } else {
                completion(false, nil)
            }
        }
    }
}
