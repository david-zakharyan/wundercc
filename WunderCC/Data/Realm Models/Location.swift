//
//  Location.swift
//  WunderCC
//
//  Created by Davit Zakharyan on 3/18/18.
//  Copyright © 2018 Davit Zakharyan. All rights reserved.
//

import RealmSwift

typealias RMLocation = RealmModelLocation

class RealmModelLocation: Object {
    @objc dynamic var name: String!
    @objc dynamic var latitude: Double = 0.0
    @objc dynamic var longitude: Double = 0.0
    @objc dynamic var address: String?
    
    /// What is the third one :)
    @objc dynamic var darthVader: Double = 0.0
    
    /// Primary key is the name
    /// Working with what we have :)
    override static func primaryKey() -> String? {
        return "name"
    }
}
