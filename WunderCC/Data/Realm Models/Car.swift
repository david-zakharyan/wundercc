//
//  Car.swift
//  WunderCC
//
//  Created by Davit Zakharyan on 3/18/18.
//  Copyright © 2018 Davit Zakharyan. All rights reserved.
//

import RealmSwift

typealias RMCar = RealmModelCar

class RealmModelCar: Object {
    @objc dynamic var vin: String?
    @objc dynamic var locationIdentifier: String?
    @objc dynamic var fuel: Int = 0
    @objc dynamic var engineType: String?
    @objc dynamic var exterior: String?
    @objc dynamic var interior: String?
    
    /// Primary key is the vin of the car
    /// Working with what we have :)
    override static func primaryKey() -> String? {
        return "vin"
    }
}
