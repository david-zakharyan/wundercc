//
//  ServiceModelNetwork.swift
//  WunderCC
//
//  Created by Davit Zakharyan on 3/17/18.
//  Copyright © 2018 Davit Zakharyan. All rights reserved.
//

// TODO: Refactor make separate file , add doc ;(
import Foundation
import Alamofire


/// Provides very basic implementation of networking
typealias SMNetwork = ServiceModelNetwork

class ServiceModelNetwork {
    
    internal static let sharedInstance = ServiceModelNetwork()
    
    internal func request(APIEndpoit: API, completion: @escaping (Bool, Any?) -> Void) {
        Alamofire.request(APIEndpoit.path, parameters: nil, headers: nil)
            .validate(statusCode: 200...299)
            .validate(contentType: [APIEndpoit.contenType])
            .responseJSON { (response) in
                if response.result.isFailure {
                    // TODO: implement proper error handling
                    completion(false, nil)
                } else {
                    completion(true, response.result.value)
                }
        }
    }
}
