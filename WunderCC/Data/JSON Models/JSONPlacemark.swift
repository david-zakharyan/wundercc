//
//  JSONPlacemark.swift
//  WunderCC
//
//  Created by Davit Zakharyan on 3/18/18.
//  Copyright © 2018 Davit Zakharyan. All rights reserved.
//

import ObjectMapper

/// Data model to represent received JSON
struct JSONPlacemark: Mappable {
    var vin: String?
    var name: String?
    var address: String?
    var coordinates: [Double]?
    var engineType: String?
    var exterior: String?
    var interior: String?
    var fuel: Int = 0
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        vin             <- map["vin"]
        name            <- map["name"]
        address         <- map["address"]
        coordinates     <- map["coordinates"]
        engineType      <- map["completed"]
        exterior        <- map["exterior"]
        fuel            <- map["fuel"]
        interior        <- map["interior"]
    }
}
