//
//  Placemark.swift
//  WunderCC
//
//  Created by Davit Zakharyan on 3/18/18.
//  Copyright © 2018 Davit Zakharyan. All rights reserved.
//

import UIKit
import MapKit

/// Custom annotation view for placemark
class PlacemarkAnnotatuionView: MKAnnotationView {
    var tapGesturRecognizer: UIGestureRecognizer?
}


/// Custom annotation for the placemark
class PlacemarkAnnotation: NSObject, MKAnnotation {
    let title: String?
    let locationAddress: String
    let coordinate: CLLocationCoordinate2D
    
    init(title: String, locationAddress: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.locationAddress = locationAddress
        self.coordinate = coordinate
        super.init()
    }
    
    var subtitle: String? {
        return locationAddress
    }
}
