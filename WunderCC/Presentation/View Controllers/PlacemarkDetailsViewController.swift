//
//  PlacemarkDetailsViewController.swift
//  WunderCC
//
//  Created by Davit Zakharyan on 3/18/18.
//  Copyright © 2018 Davit Zakharyan. All rights reserved.
//

import UIKit

//TODO: diplay all data and map with only this location
//TODO: open maps application from the controller
class PlacemarkDetailsViewController: ViewController {

    var car: RMCar?
    var location: RMLocation?
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelFuel: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setup() {
        if let car = car {
            // TODO: get the location - add location wrapper
            labelName.text = "Name : " + car.locationIdentifier!
            labelFuel.text = "Fuel : " + String(describing: car.fuel)
        }
        if let location = location {
            // TODO: get the car - add car wrapper
            labelAddress.text = "Address : " + location.address!
        }
    }
}
