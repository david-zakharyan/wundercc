//
//  MapViewController.swift
//  WunderCC
//
//  Created by Davit Zakharyan on 3/18/18.
//  Copyright © 2018 Davit Zakharyan. All rights reserved.
//

import UIKit
import MapKit
import RealmSwift

class MapViewController: ViewController {

    @IBOutlet weak var mapView: MKMapView!
    
    fileprivate var selectedPlacemark: PlacemarkAnnotatuionView?
    
    private let locationManager = CLLocationManager()
    private let regionRadius: CLLocationDistance = 1_000
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        locationManager.distanceFilter = regionRadius
        locationManager.delegate = self
        checkLocationAuthorizationStatus()
        setupLocationDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @objc func recognizedOnAnnotationGesture(sender: UITapGestureRecognizer) {
        mapView.deselectAnnotation(selectedPlacemark?.annotation, animated: true)
        showAnnotations()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "locationDetailsSegue", let detailsVC = segue.destination as? PlacemarkDetailsViewController {
            
        }
    }
    
    // MARK: - Private methods
    
    /// Hides all annotation except the selected one
    private func hideAnnotations() {
        let views = visibleAnnotationsViews()
        setAnnotationsViews(views, visible: false)
    }
    
    /// displays all annoatations
    private func showAnnotations() {
        let views = visibleAnnotationsViews()
        setAnnotationsViews(views, visible: true)
    }
    
    /// Returns the annotation views that are visible
    private func visibleAnnotationsViews() -> [MKAnnotationView] {
        let visibleAnnotationsRect: MKMapRect = mapView.visibleMapRect
        
        var annotationsViews: [MKAnnotationView] = []
        for annotation in mapView.annotations(in: visibleAnnotationsRect) where annotation is MKAnnotation {
            if let annotationView = mapView.view(for: annotation as! MKAnnotation) {
                annotationsViews.append(annotationView)
            }
        }
        
        return annotationsViews
    }
    
    private func setAnnotationsViews(_ views: [MKAnnotationView], visible visibilty: Bool) {
        for view in views where view != selectedPlacemark {
            DispatchQueue.main.async {
                if view.isHidden == visibilty {
                    view.isHidden = !visibilty
                }
            }
        }
    }
    
    /// Sets up the data source for the annotations
    private func setupLocationDataSource() {
        // FIXME: chrash me please :)
        let locationsResult = try! Realm()
        
        var locations: [PlacemarkAnnotation] = []
        
        for location in locationsResult.objects(RMLocation.self) {
            let annotation = PlacemarkAnnotation(title: location.name,
                                                 locationAddress: location.address ?? "",
                                                 coordinate: CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude))
            locations.append(annotation)
        }
        
        if locations.count > 0 {
            mapView.addAnnotations(locations)
        }
    }
    
    
    /// Centers the map on the gven location
    ///
    /// - Parameter location: location regarding to which the map should be centered
    private func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    /// Requests or sets the location manager authorization
    private func checkLocationAuthorizationStatus() {
        // TODO: Revoked access or rejected - show popup navigating to settings
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            mapView.showsUserLocation = true
            if let userLocation = locationManager.location {
                centerMapOnLocation(location: userLocation)
            }
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    fileprivate func openDetailsViewController() {
        performSegue(withIdentifier: "locationDetailsSegue", sender: nil)
    }
}

// MARK: - CLLocationManagerDelegate

extension MapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        centerMapOnLocation(location: locations.first!)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        // TODO: Porper error handling
    }
}

// MARK: - MKMapViewDelegate

extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation { return nil }
        let identifier = "AnnotationIdentifier"
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        if annotationView == nil {
            annotationView = PlacemarkAnnotatuionView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            annotationView?.canShowCallout = true
            annotationView?.image = UIImage(named: "placemarkLocationPin")
            annotationView?.isHidden = (selectedPlacemark != nil)
        }
        else {
            annotationView?.annotation = annotation
            annotationView?.isHidden = (selectedPlacemark != nil)
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let placemark = view as? PlacemarkAnnotatuionView, selectedPlacemark != view {
            let tapGesturRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.recognizedOnAnnotationGesture(sender:)))
            placemark.addGestureRecognizer(tapGesturRecognizer)
            placemark.tapGesturRecognizer = tapGesturRecognizer
            selectedPlacemark = placemark
            hideAnnotations()
        } else {
            selectedPlacemark = nil
            mapView.deselectAnnotation(view.annotation, animated: false)
        }
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if let placemark = view as? PlacemarkAnnotatuionView {
            placemark.tapGesturRecognizer = nil
        }
    }
    
    // TODO: Find a better solution - this doesn't work XD
    // will clean previous region
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        if selectedPlacemark == nil {
            showAnnotations()
        } else {
            hideAnnotations()
        }
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        openDetailsViewController()
    }
}
