//
//  PlacemarkTableViewController.swift
//  WunderCC
//
//  Created by Davit Zakharyan on 3/18/18.
//  Copyright © 2018 Davit Zakharyan. All rights reserved.
//

import UIKit
import RealmSwift

class PlacemarkTableViewController: ViewController {

    @IBOutlet weak var tableView: UITableView!
    fileprivate var dataSource: Results<RMCar>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDataSource()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Private methods
    
    private func setupDataSource() {
        // FIXME: chrash me please :)
        let todoRealm = try! Realm()
        dataSource = todoRealm.objects(RMCar.self)
        
        if let count = dataSource?.count, count > 0 {
            tableView.reloadData()
        }
    }
    
    fileprivate func openDetailsViewController() {
        performSegue(withIdentifier: "placemarkDetailsSegue", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "placemarkDetailsSegue", let detailsVC = segue.destination as? PlacemarkDetailsViewController {
            if let indexPath = tableView.indexPathForSelectedRow {
                let car = dataSource![indexPath.row]
                detailsVC.car = car
                tableView.deselectRow(at: indexPath, animated: true)
            }
        }
    }
}

// MARK: - UITableViewDataSource
extension PlacemarkTableViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "placemarkCellIdentifier") ?? UITableViewCell()
        
        let car = dataSource![indexPath.row]
        cell.textLabel?.text = car.locationIdentifier
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension PlacemarkTableViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        openDetailsViewController()
    }
}
