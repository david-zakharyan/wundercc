//
//  LandingViewController.swift
//  WunderCC
//
//  Created by Davit Zakharyan on 3/17/18.
//  Copyright © 2018 Davit Zakharyan. All rights reserved.
//

import UIKit

class LandingViewController: ViewController {

    @IBOutlet weak var imageViewFaceLogo: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        animateLogoAppearance()
        requestAvailablePlacemarks()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        imageViewFaceLogo.layer.removeAllAnimations()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Private methods
    private func start() {
        self.performSegue(withIdentifier: "tabBarInitializationSegue", sender: nil)
    }
    
    private func animateLogoAppearance() {
        UIView.animate(withDuration: 1.5, delay: 0, options: [.repeat,.autoreverse], animations: { [weak self] in
            self?.imageViewFaceLogo.alpha = 1.0
            print("fuc")
        }, completion: nil)
    }
    
    private func requestAvailablePlacemarks() {
        DAOPlacemarks().placmarks { [weak self] (success, data) in
            if data != nil {
                if SMDatabase().save(placemarks: data!) {
                    self?.start()
                }
                // TODO: Introduce retry option if faild
            }
        }
    }
}
