//
//  ViewController.swift
//  WunderCC
//
//  Created by Davit Zakharyan on 3/17/18.
//  Copyright © 2018 Davit Zakharyan. All rights reserved.
//

import UIKit


/// Will serve as base view controller
class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

